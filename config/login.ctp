<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?=$this->Html->css('bootstrap.min');?>
    <?=$this->Html->script('jquery.js')?>
    <title>Consultório Livre :: Login</title>
    <title>Consultório Livre</title>
    <style>
    @import url(https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,500i,600,600i,700,700i,800);
    *{
        font-family: 'Poppins', sans-serif !important;
    }
    html * {
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }    
    div.col-md-12{
        margin-bottom:15px;
    }
    </style>
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body style="margin:0px;padding:0px;background-image:url(/img/fundo_landing.webp);background-position:top center;background-size:cover;">
    <section style="width:100%;padding:3%;margin:0;height:calc(100vh);background-color:rgba(0,0,0,.8);color:#FFF;">
        <figure style="width:99%;text-align:center;margin-top:50px">
            <?=$this->Html->image('livre-01.svg',['style'=>'width:25%;margin-bottom:40px']);?>
        </figure>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <fieldset>
                    <legend style="color:#fff;">Bem vindo Dr(a)</legend>
                    <?= $this->Flash->render('login') ?>
                    <?php
                    echo $this->Form->create();
                    ?>
                    <div class="row">
                        <div class="col-md-12">
                        <?php
                        echo $this->Form->control('cpf',['label'=>false,'placeholder'=>'E-mail ou CPF','class'=>'form-control']);
                        ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                        <?php
                        echo $this->Form->control('password',['label'=>false,'placeholder'=>'Sua senha', 'class'=>'form-control']);
                        ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                        <?php
                        echo $this->Form->button('Entrar',['class'=>'btn btn-success']);
                        echo $this->Html->link('Esqueci a senha',['controller'=>'Usuarios','action'=>'esqueci'],['class'=>'btn btn-default', 'style'=>'margin-left:10px;']);
                        ?>
                        </div>
                        <div class="col-md-12">
                            <?=$this->Html->link('<span style="color:#000">Ainda não tem conta?</span> Cadastre-se aqui','https://www.consultoriolivre.com.br/registre-se',['class'=>'btn btn-warning btn-block','escape'=>false]);?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                    echo $this->Form->end();
                    ?>
                </fieldset>
            </div>
        </div>
    </section>
</body>
</html>