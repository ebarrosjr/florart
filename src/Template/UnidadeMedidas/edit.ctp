<div class="card">
    <div class="card-header">
        <h4 class="card-title">Editar Unidade de Medidas</h4>
    </div>
    <div class="card-body">
        <?= $this->Form->create($um) ?>
        <div class="row">
            <div class="col-md-8">
            <?php
                echo $this->Form->control('nome',['class'=>'form-control']);
            ?>
            </div>
            <div class="col-md-4">
            <?php
                echo $this->Form->control('sigla',['class'=>'form-control']);
            ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
            <?php
                echo $this->Form->control('parent_id',['label'=>'Unidade base', 'options'=>$unidades, 'empty'=>' - Não aplicável - ','class'=>'form-control']);
            ?>
            </div>
            <div class="col-md-4">
            <?php
                echo $this->Form->control('fator_multiplicativo',['class'=>'form-control']);
            ?>
            </div>
            <div class="col-md-4 pt-5 mt-1">
                <?= $this->Form->button(' Gravar ', ['class'=>'btn btn-success btn-block']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
