<div class="col-12">
    <div class="row">
        <div class="col-12">
            <h3><?=$produto->nome?></h3>
        </div>
        <div class="col-2 card-panel">
            <span class="card-panel-numero"><?=$pedidos->count();?></span>
            Pedidos
        </div>
        <div class="col-2 card-panel">
            vendas <?=$pedidos->count();?>
        </div>
        <div class="col-2 card-panel">
            custo médio <?=$pedidos->count();?>
        </div>
        <div class="col-2 card-panel">
            lucro médio <?=$pedidos->count();?>
        </div>
        <div class="col-2 card-panel">
            Taxa de retorno <?=$pedidos->count();?>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Pedidos realizados</h3>
        </div>
        <div class="card-body">
            <table class="table card-table table-vcenter">
                <thead>
                    <tr>
                        <th scope="col">Data</th>
                        <th scope="col">Cliente</th>
                        <th scope="col">Quantidade</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>