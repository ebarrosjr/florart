<div class="col-12">
    <div class="card">
        <?= $this->Form->create($produto) ?>
        <div class="card-header">
            <h3 class="card-title">Novo produto</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-3">
                <?php
                echo $this->Form->control('tipo_produto_id', ['options' => $tipoProdutos,'class'=>'form-control']);
                ?>
                </div>
                <div class="col-md-3">
                <?php
                echo $this->Form->control('grupo_produto_id', ['options' => $grupoProdutos, 'empty' =>' - Não aplicável - ','class'=>'form-control']);
                ?>
                </div>
                <div class="col-md-6">
                    <?php
                    echo $this->Form->control('nome',['class'=>'form-control']);
                    ?>
                </div>
                <div class="col-md-3">
                <label for="valor_atacado">Valor para varejo</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> R$ </span>
                        </div>
                        <?php
                        echo $this->Form->control('valor_varejo',['label'=>false,'class'=>'form-control']);
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <label for="valor_atacado">Valor para atacado</label>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text"> R$ </span>
                        </div>
                        <?php
                        echo $this->Form->control('valor_atacado',['label'=>false,'class'=>'form-control']);
                        ?>
                    </div>
                </div>
                <div class="col-md-3">
                <?php
                echo $this->Form->control('estoque_minimo',['class'=>'form-control']);
                ?>
                </div>
                <div class="col-md-3">
                <?php
                echo $this->Form->control('atacado_minimo',['class'=>'form-control']);
                ?>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <?=$this->Form->button(' Gravar ', ['class'=>'btn btn-success']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div>
