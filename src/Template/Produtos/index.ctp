<div class="col-12">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Produtos<?=$this->Html->link('<i class="fe fe-plus"></i> Novo', ['action'=>'add'],['class'=>'btn btn-pill btn-success btn-sm float-right','escape'=>false])?></h3>
        </div>
        <div class="card-body">
            <table class="table card-table table-vcenter">
                <thead>
                    <tr>
                        <th scope="col">Grupo</th>
                        <th scope="col">nome</th>
                        <th class="text-center" scope="col">Valor varejo</th>
                        <th class="text-center" scope="col">Valor atacado</th>
                        <th class="text-center" scope="col">Estoque mínimo</th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($produtos as $produto): ?>
                    <tr>
                        <td><?= $produto->has('grupo_produto') ? $produto->grupo_produto->nome : '' ?></td>
                        <td><?= h($produto->nome) ?></td>
                        <td class="text-center"> R$ <?= number_format($produto->valor_varejo,2,',','.') ?></td>
                        <td class="text-center"> R$ <?= number_format($produto->valor_atacado,2,',','.') ?></td>
                        <td class="text-center"> <?= number_format($produto->estoque_minimo,2,',','.') ?></td>
                        <td class="actions">
                            <?= $this->Html->link('<i class="fe fe-eye"></i>', ['action' => 'view', $produto->id], ['escape'=>false]) ?>
                            <?= $this->Html->link('<i class="fe fe-edit"></i>', ['action' => 'edit', $produto->id],['escape'=>false]) ?>
                            <?= $this->Form->postLink('<i class="fe fe-trash"></i>', ['action' => 'delete', $produto->id], ['confirm' => __('Are you sure you want to delete {0}?', $produto->nome),'escape'=>false]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="d-flex align-items-center mb-4">
        <ul class="pagination">
            <?= $this->Paginator->first('<<') ?>
            <?= $this->Paginator->prev('<') ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next('>') ?>
            <?= $this->Paginator->last('>>') ?>
        </ul>
        <div class="page-total-text"><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></div>
    </div>
</div>
 